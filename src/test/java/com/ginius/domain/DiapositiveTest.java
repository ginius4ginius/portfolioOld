package com.ginius.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ginius.web.rest.TestUtil;

public class DiapositiveTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Diapositive.class);
        Diapositive diapositive1 = new Diapositive();
        diapositive1.setId(1L);
        Diapositive diapositive2 = new Diapositive();
        diapositive2.setId(diapositive1.getId());
        assertThat(diapositive1).isEqualTo(diapositive2);
        diapositive2.setId(2L);
        assertThat(diapositive1).isNotEqualTo(diapositive2);
        diapositive1.setId(null);
        assertThat(diapositive1).isNotEqualTo(diapositive2);
    }
}
