package com.ginius.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ginius.web.rest.TestUtil;

public class PortfolioConfigurationTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PortfolioConfiguration.class);
        PortfolioConfiguration portfolioConfiguration1 = new PortfolioConfiguration();
        portfolioConfiguration1.setId(1L);
        PortfolioConfiguration portfolioConfiguration2 = new PortfolioConfiguration();
        portfolioConfiguration2.setId(portfolioConfiguration1.getId());
        assertThat(portfolioConfiguration1).isEqualTo(portfolioConfiguration2);
        portfolioConfiguration2.setId(2L);
        assertThat(portfolioConfiguration1).isNotEqualTo(portfolioConfiguration2);
        portfolioConfiguration1.setId(null);
        assertThat(portfolioConfiguration1).isNotEqualTo(portfolioConfiguration2);
    }
}
