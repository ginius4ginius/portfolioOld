package com.ginius.web.rest;

import com.ginius.PortfolioApp;
import com.ginius.domain.Diapositive;
import com.ginius.repository.DiapositiveRepository;
import com.ginius.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.ginius.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DiapositiveResource} REST controller.
 */
@SpringBootTest(classes = PortfolioApp.class)
public class DiapositiveResourceIT {

    private static final String DEFAULT_PHOTO_URL = "AAAAAAAAAA";
    private static final String UPDATED_PHOTO_URL = "BBBBBBBBBB";

    private static final String DEFAULT_CONTENU = "AAAAAAAAAA";
    private static final String UPDATED_CONTENU = "BBBBBBBBBB";

    @Autowired
    private DiapositiveRepository diapositiveRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDiapositiveMockMvc;

    private Diapositive diapositive;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DiapositiveResource diapositiveResource = new DiapositiveResource(diapositiveRepository);
        this.restDiapositiveMockMvc = MockMvcBuilders.standaloneSetup(diapositiveResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Diapositive createEntity(EntityManager em) {
        Diapositive diapositive = new Diapositive()
            .photoUrl(DEFAULT_PHOTO_URL)
            .contenu(DEFAULT_CONTENU);
        return diapositive;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Diapositive createUpdatedEntity(EntityManager em) {
        Diapositive diapositive = new Diapositive()
            .photoUrl(UPDATED_PHOTO_URL)
            .contenu(UPDATED_CONTENU);
        return diapositive;
    }

    @BeforeEach
    public void initTest() {
        diapositive = createEntity(em);
    }

    @Test
    @Transactional
    public void createDiapositive() throws Exception {
        int databaseSizeBeforeCreate = diapositiveRepository.findAll().size();

        // Create the Diapositive
        restDiapositiveMockMvc.perform(post("/api/diapositives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diapositive)))
            .andExpect(status().isCreated());

        // Validate the Diapositive in the database
        List<Diapositive> diapositiveList = diapositiveRepository.findAll();
        assertThat(diapositiveList).hasSize(databaseSizeBeforeCreate + 1);
        Diapositive testDiapositive = diapositiveList.get(diapositiveList.size() - 1);
        assertThat(testDiapositive.getPhotoUrl()).isEqualTo(DEFAULT_PHOTO_URL);
        assertThat(testDiapositive.getContenu()).isEqualTo(DEFAULT_CONTENU);
    }

    @Test
    @Transactional
    public void createDiapositiveWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = diapositiveRepository.findAll().size();

        // Create the Diapositive with an existing ID
        diapositive.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDiapositiveMockMvc.perform(post("/api/diapositives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diapositive)))
            .andExpect(status().isBadRequest());

        // Validate the Diapositive in the database
        List<Diapositive> diapositiveList = diapositiveRepository.findAll();
        assertThat(diapositiveList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkPhotoUrlIsRequired() throws Exception {
        int databaseSizeBeforeTest = diapositiveRepository.findAll().size();
        // set the field null
        diapositive.setPhotoUrl(null);

        // Create the Diapositive, which fails.

        restDiapositiveMockMvc.perform(post("/api/diapositives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diapositive)))
            .andExpect(status().isBadRequest());

        List<Diapositive> diapositiveList = diapositiveRepository.findAll();
        assertThat(diapositiveList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContenuIsRequired() throws Exception {
        int databaseSizeBeforeTest = diapositiveRepository.findAll().size();
        // set the field null
        diapositive.setContenu(null);

        // Create the Diapositive, which fails.

        restDiapositiveMockMvc.perform(post("/api/diapositives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diapositive)))
            .andExpect(status().isBadRequest());

        List<Diapositive> diapositiveList = diapositiveRepository.findAll();
        assertThat(diapositiveList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDiapositives() throws Exception {
        // Initialize the database
        diapositiveRepository.saveAndFlush(diapositive);

        // Get all the diapositiveList
        restDiapositiveMockMvc.perform(get("/api/diapositives?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(diapositive.getId().intValue())))
            .andExpect(jsonPath("$.[*].photoUrl").value(hasItem(DEFAULT_PHOTO_URL)))
            .andExpect(jsonPath("$.[*].contenu").value(hasItem(DEFAULT_CONTENU)));
    }
    
    @Test
    @Transactional
    public void getDiapositive() throws Exception {
        // Initialize the database
        diapositiveRepository.saveAndFlush(diapositive);

        // Get the diapositive
        restDiapositiveMockMvc.perform(get("/api/diapositives/{id}", diapositive.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(diapositive.getId().intValue()))
            .andExpect(jsonPath("$.photoUrl").value(DEFAULT_PHOTO_URL))
            .andExpect(jsonPath("$.contenu").value(DEFAULT_CONTENU));
    }

    @Test
    @Transactional
    public void getNonExistingDiapositive() throws Exception {
        // Get the diapositive
        restDiapositiveMockMvc.perform(get("/api/diapositives/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDiapositive() throws Exception {
        // Initialize the database
        diapositiveRepository.saveAndFlush(diapositive);

        int databaseSizeBeforeUpdate = diapositiveRepository.findAll().size();

        // Update the diapositive
        Diapositive updatedDiapositive = diapositiveRepository.findById(diapositive.getId()).get();
        // Disconnect from session so that the updates on updatedDiapositive are not directly saved in db
        em.detach(updatedDiapositive);
        updatedDiapositive
            .photoUrl(UPDATED_PHOTO_URL)
            .contenu(UPDATED_CONTENU);

        restDiapositiveMockMvc.perform(put("/api/diapositives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDiapositive)))
            .andExpect(status().isOk());

        // Validate the Diapositive in the database
        List<Diapositive> diapositiveList = diapositiveRepository.findAll();
        assertThat(diapositiveList).hasSize(databaseSizeBeforeUpdate);
        Diapositive testDiapositive = diapositiveList.get(diapositiveList.size() - 1);
        assertThat(testDiapositive.getPhotoUrl()).isEqualTo(UPDATED_PHOTO_URL);
        assertThat(testDiapositive.getContenu()).isEqualTo(UPDATED_CONTENU);
    }

    @Test
    @Transactional
    public void updateNonExistingDiapositive() throws Exception {
        int databaseSizeBeforeUpdate = diapositiveRepository.findAll().size();

        // Create the Diapositive

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDiapositiveMockMvc.perform(put("/api/diapositives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(diapositive)))
            .andExpect(status().isBadRequest());

        // Validate the Diapositive in the database
        List<Diapositive> diapositiveList = diapositiveRepository.findAll();
        assertThat(diapositiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDiapositive() throws Exception {
        // Initialize the database
        diapositiveRepository.saveAndFlush(diapositive);

        int databaseSizeBeforeDelete = diapositiveRepository.findAll().size();

        // Delete the diapositive
        restDiapositiveMockMvc.perform(delete("/api/diapositives/{id}", diapositive.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Diapositive> diapositiveList = diapositiveRepository.findAll();
        assertThat(diapositiveList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
