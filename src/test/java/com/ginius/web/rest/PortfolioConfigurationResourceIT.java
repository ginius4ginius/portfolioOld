package com.ginius.web.rest;

import com.ginius.PortfolioApp;
import com.ginius.domain.PortfolioConfiguration;
import com.ginius.repository.PortfolioConfigurationRepository;
import com.ginius.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.ginius.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PortfolioConfigurationResource} REST controller.
 */
@SpringBootTest(classes = PortfolioApp.class)
public class PortfolioConfigurationResourceIT {

    private static final String DEFAULT_CV_URL = "AAAAAAAAAA";
    private static final String UPDATED_CV_URL = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PRESENTATION = "AAAAAAAAAA";
    private static final String UPDATED_PRESENTATION = "BBBBBBBBBB";

    private static final String DEFAULT_PROFILE_URL = "AAAAAAAAAA";
    private static final String UPDATED_PROFILE_URL = "BBBBBBBBBB";

    @Autowired
    private PortfolioConfigurationRepository portfolioConfigurationRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPortfolioConfigurationMockMvc;

    private PortfolioConfiguration portfolioConfiguration;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PortfolioConfigurationResource portfolioConfigurationResource = new PortfolioConfigurationResource(portfolioConfigurationRepository);
        this.restPortfolioConfigurationMockMvc = MockMvcBuilders.standaloneSetup(portfolioConfigurationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PortfolioConfiguration createEntity(EntityManager em) {
        PortfolioConfiguration portfolioConfiguration = new PortfolioConfiguration()
            .cvUrl(DEFAULT_CV_URL)
            .email(DEFAULT_EMAIL)
            .presentation(DEFAULT_PRESENTATION)
            .profileUrl(DEFAULT_PROFILE_URL);
        return portfolioConfiguration;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PortfolioConfiguration createUpdatedEntity(EntityManager em) {
        PortfolioConfiguration portfolioConfiguration = new PortfolioConfiguration()
            .cvUrl(UPDATED_CV_URL)
            .email(UPDATED_EMAIL)
            .presentation(UPDATED_PRESENTATION)
            .profileUrl(UPDATED_PROFILE_URL);
        return portfolioConfiguration;
    }

    @BeforeEach
    public void initTest() {
        portfolioConfiguration = createEntity(em);
    }

    @Test
    @Transactional
    public void createPortfolioConfiguration() throws Exception {
        int databaseSizeBeforeCreate = portfolioConfigurationRepository.findAll().size();

        // Create the PortfolioConfiguration
        restPortfolioConfigurationMockMvc.perform(post("/api/portfolio-configurations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(portfolioConfiguration)))
            .andExpect(status().isCreated());

        // Validate the PortfolioConfiguration in the database
        List<PortfolioConfiguration> portfolioConfigurationList = portfolioConfigurationRepository.findAll();
        assertThat(portfolioConfigurationList).hasSize(databaseSizeBeforeCreate + 1);
        PortfolioConfiguration testPortfolioConfiguration = portfolioConfigurationList.get(portfolioConfigurationList.size() - 1);
        assertThat(testPortfolioConfiguration.getCvUrl()).isEqualTo(DEFAULT_CV_URL);
        assertThat(testPortfolioConfiguration.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testPortfolioConfiguration.getPresentation()).isEqualTo(DEFAULT_PRESENTATION);
        assertThat(testPortfolioConfiguration.getProfileUrl()).isEqualTo(DEFAULT_PROFILE_URL);
    }

    @Test
    @Transactional
    public void createPortfolioConfigurationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = portfolioConfigurationRepository.findAll().size();

        // Create the PortfolioConfiguration with an existing ID
        portfolioConfiguration.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPortfolioConfigurationMockMvc.perform(post("/api/portfolio-configurations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(portfolioConfiguration)))
            .andExpect(status().isBadRequest());

        // Validate the PortfolioConfiguration in the database
        List<PortfolioConfiguration> portfolioConfigurationList = portfolioConfigurationRepository.findAll();
        assertThat(portfolioConfigurationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = portfolioConfigurationRepository.findAll().size();
        // set the field null
        portfolioConfiguration.setEmail(null);

        // Create the PortfolioConfiguration, which fails.

        restPortfolioConfigurationMockMvc.perform(post("/api/portfolio-configurations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(portfolioConfiguration)))
            .andExpect(status().isBadRequest());

        List<PortfolioConfiguration> portfolioConfigurationList = portfolioConfigurationRepository.findAll();
        assertThat(portfolioConfigurationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPortfolioConfigurations() throws Exception {
        // Initialize the database
        portfolioConfigurationRepository.saveAndFlush(portfolioConfiguration);

        // Get all the portfolioConfigurationList
        restPortfolioConfigurationMockMvc.perform(get("/api/portfolio-configurations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(portfolioConfiguration.getId().intValue())))
            .andExpect(jsonPath("$.[*].cvUrl").value(hasItem(DEFAULT_CV_URL)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].presentation").value(hasItem(DEFAULT_PRESENTATION)))
            .andExpect(jsonPath("$.[*].profileUrl").value(hasItem(DEFAULT_PROFILE_URL)));
    }
    
    @Test
    @Transactional
    public void getPortfolioConfiguration() throws Exception {
        // Initialize the database
        portfolioConfigurationRepository.saveAndFlush(portfolioConfiguration);

        // Get the portfolioConfiguration
        restPortfolioConfigurationMockMvc.perform(get("/api/portfolio-configurations/{id}", portfolioConfiguration.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(portfolioConfiguration.getId().intValue()))
            .andExpect(jsonPath("$.cvUrl").value(DEFAULT_CV_URL))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.presentation").value(DEFAULT_PRESENTATION))
            .andExpect(jsonPath("$.profileUrl").value(DEFAULT_PROFILE_URL));
    }

    @Test
    @Transactional
    public void getNonExistingPortfolioConfiguration() throws Exception {
        // Get the portfolioConfiguration
        restPortfolioConfigurationMockMvc.perform(get("/api/portfolio-configurations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePortfolioConfiguration() throws Exception {
        // Initialize the database
        portfolioConfigurationRepository.saveAndFlush(portfolioConfiguration);

        int databaseSizeBeforeUpdate = portfolioConfigurationRepository.findAll().size();

        // Update the portfolioConfiguration
        PortfolioConfiguration updatedPortfolioConfiguration = portfolioConfigurationRepository.findById(portfolioConfiguration.getId()).get();
        // Disconnect from session so that the updates on updatedPortfolioConfiguration are not directly saved in db
        em.detach(updatedPortfolioConfiguration);
        updatedPortfolioConfiguration
            .cvUrl(UPDATED_CV_URL)
            .email(UPDATED_EMAIL)
            .presentation(UPDATED_PRESENTATION)
            .profileUrl(UPDATED_PROFILE_URL);

        restPortfolioConfigurationMockMvc.perform(put("/api/portfolio-configurations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPortfolioConfiguration)))
            .andExpect(status().isOk());

        // Validate the PortfolioConfiguration in the database
        List<PortfolioConfiguration> portfolioConfigurationList = portfolioConfigurationRepository.findAll();
        assertThat(portfolioConfigurationList).hasSize(databaseSizeBeforeUpdate);
        PortfolioConfiguration testPortfolioConfiguration = portfolioConfigurationList.get(portfolioConfigurationList.size() - 1);
        assertThat(testPortfolioConfiguration.getCvUrl()).isEqualTo(UPDATED_CV_URL);
        assertThat(testPortfolioConfiguration.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testPortfolioConfiguration.getPresentation()).isEqualTo(UPDATED_PRESENTATION);
        assertThat(testPortfolioConfiguration.getProfileUrl()).isEqualTo(UPDATED_PROFILE_URL);
    }

    @Test
    @Transactional
    public void updateNonExistingPortfolioConfiguration() throws Exception {
        int databaseSizeBeforeUpdate = portfolioConfigurationRepository.findAll().size();

        // Create the PortfolioConfiguration

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPortfolioConfigurationMockMvc.perform(put("/api/portfolio-configurations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(portfolioConfiguration)))
            .andExpect(status().isBadRequest());

        // Validate the PortfolioConfiguration in the database
        List<PortfolioConfiguration> portfolioConfigurationList = portfolioConfigurationRepository.findAll();
        assertThat(portfolioConfigurationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePortfolioConfiguration() throws Exception {
        // Initialize the database
        portfolioConfigurationRepository.saveAndFlush(portfolioConfiguration);

        int databaseSizeBeforeDelete = portfolioConfigurationRepository.findAll().size();

        // Delete the portfolioConfiguration
        restPortfolioConfigurationMockMvc.perform(delete("/api/portfolio-configurations/{id}", portfolioConfiguration.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PortfolioConfiguration> portfolioConfigurationList = portfolioConfigurationRepository.findAll();
        assertThat(portfolioConfigurationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
