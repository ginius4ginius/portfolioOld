import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PortfolioTestModule } from '../../../test.module';
import { PortfolioConfigurationUpdateComponent } from 'app/entities/portfolio-configuration/portfolio-configuration-update.component';
import { PortfolioConfigurationService } from 'app/entities/portfolio-configuration/portfolio-configuration.service';
import { PortfolioConfiguration } from 'app/shared/model/portfolio-configuration.model';

describe('Component Tests', () => {
  describe('PortfolioConfiguration Management Update Component', () => {
    let comp: PortfolioConfigurationUpdateComponent;
    let fixture: ComponentFixture<PortfolioConfigurationUpdateComponent>;
    let service: PortfolioConfigurationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PortfolioTestModule],
        declarations: [PortfolioConfigurationUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(PortfolioConfigurationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PortfolioConfigurationUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PortfolioConfigurationService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new PortfolioConfiguration(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new PortfolioConfiguration();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
