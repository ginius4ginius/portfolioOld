import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PortfolioTestModule } from '../../../test.module';
import { PortfolioConfigurationComponent } from 'app/entities/portfolio-configuration/portfolio-configuration.component';
import { PortfolioConfigurationService } from 'app/entities/portfolio-configuration/portfolio-configuration.service';
import { PortfolioConfiguration } from 'app/shared/model/portfolio-configuration.model';

describe('Component Tests', () => {
  describe('PortfolioConfiguration Management Component', () => {
    let comp: PortfolioConfigurationComponent;
    let fixture: ComponentFixture<PortfolioConfigurationComponent>;
    let service: PortfolioConfigurationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PortfolioTestModule],
        declarations: [PortfolioConfigurationComponent],
        providers: []
      })
        .overrideTemplate(PortfolioConfigurationComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PortfolioConfigurationComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PortfolioConfigurationService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new PortfolioConfiguration(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.portfolioConfigurations && comp.portfolioConfigurations[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
