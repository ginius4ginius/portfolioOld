import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import { PortfolioConfigurationService } from 'app/entities/portfolio-configuration/portfolio-configuration.service';
import { IPortfolioConfiguration, PortfolioConfiguration } from 'app/shared/model/portfolio-configuration.model';

describe('Service Tests', () => {
  describe('PortfolioConfiguration Service', () => {
    let injector: TestBed;
    let service: PortfolioConfigurationService;
    let httpMock: HttpTestingController;
    let elemDefault: IPortfolioConfiguration;
    let expectedResult: IPortfolioConfiguration | IPortfolioConfiguration[] | boolean | null;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(PortfolioConfigurationService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new PortfolioConfiguration(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a PortfolioConfiguration', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .create(new PortfolioConfiguration())
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a PortfolioConfiguration', () => {
        const returnedFromService = Object.assign(
          {
            cvUrl: 'BBBBBB',
            email: 'BBBBBB',
            presentation: 'BBBBBB',
            profileUrl: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp.body));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of PortfolioConfiguration', () => {
        const returnedFromService = Object.assign(
          {
            cvUrl: 'BBBBBB',
            email: 'BBBBBB',
            presentation: 'BBBBBB',
            profileUrl: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .query()
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a PortfolioConfiguration', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
