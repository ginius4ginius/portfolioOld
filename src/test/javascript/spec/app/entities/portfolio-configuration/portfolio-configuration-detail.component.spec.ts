import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PortfolioTestModule } from '../../../test.module';
import { PortfolioConfigurationDetailComponent } from 'app/entities/portfolio-configuration/portfolio-configuration-detail.component';
import { PortfolioConfiguration } from 'app/shared/model/portfolio-configuration.model';

describe('Component Tests', () => {
  describe('PortfolioConfiguration Management Detail Component', () => {
    let comp: PortfolioConfigurationDetailComponent;
    let fixture: ComponentFixture<PortfolioConfigurationDetailComponent>;
    const route = ({ data: of({ portfolioConfiguration: new PortfolioConfiguration(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PortfolioTestModule],
        declarations: [PortfolioConfigurationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(PortfolioConfigurationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PortfolioConfigurationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load portfolioConfiguration on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.portfolioConfiguration).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
