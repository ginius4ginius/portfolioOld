import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PortfolioTestModule } from '../../../test.module';
import { DiapositiveDetailComponent } from 'app/entities/diapositive/diapositive-detail.component';
import { Diapositive } from 'app/shared/model/diapositive.model';

describe('Component Tests', () => {
  describe('Diapositive Management Detail Component', () => {
    let comp: DiapositiveDetailComponent;
    let fixture: ComponentFixture<DiapositiveDetailComponent>;
    const route = ({ data: of({ diapositive: new Diapositive(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PortfolioTestModule],
        declarations: [DiapositiveDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(DiapositiveDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DiapositiveDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load diapositive on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.diapositive).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
