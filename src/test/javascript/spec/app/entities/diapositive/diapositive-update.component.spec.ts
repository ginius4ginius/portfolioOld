import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PortfolioTestModule } from '../../../test.module';
import { DiapositiveUpdateComponent } from 'app/entities/diapositive/diapositive-update.component';
import { DiapositiveService } from 'app/entities/diapositive/diapositive.service';
import { Diapositive } from 'app/shared/model/diapositive.model';

describe('Component Tests', () => {
  describe('Diapositive Management Update Component', () => {
    let comp: DiapositiveUpdateComponent;
    let fixture: ComponentFixture<DiapositiveUpdateComponent>;
    let service: DiapositiveService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PortfolioTestModule],
        declarations: [DiapositiveUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(DiapositiveUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DiapositiveUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DiapositiveService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Diapositive(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Diapositive();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
