import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PortfolioTestModule } from '../../../test.module';
import { DiapositiveComponent } from 'app/entities/diapositive/diapositive.component';
import { DiapositiveService } from 'app/entities/diapositive/diapositive.service';
import { Diapositive } from 'app/shared/model/diapositive.model';

describe('Component Tests', () => {
  describe('Diapositive Management Component', () => {
    let comp: DiapositiveComponent;
    let fixture: ComponentFixture<DiapositiveComponent>;
    let service: DiapositiveService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PortfolioTestModule],
        declarations: [DiapositiveComponent],
        providers: []
      })
        .overrideTemplate(DiapositiveComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DiapositiveComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DiapositiveService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Diapositive(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.diapositives && comp.diapositives[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
