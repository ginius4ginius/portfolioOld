package com.ginius.web.rest;

import com.ginius.domain.Diapositive;
import com.ginius.repository.DiapositiveRepository;
import com.ginius.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ginius.domain.Diapositive}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class DiapositiveResource {

    private final Logger log = LoggerFactory.getLogger(DiapositiveResource.class);

    private static final String ENTITY_NAME = "diapositive";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DiapositiveRepository diapositiveRepository;

    public DiapositiveResource(DiapositiveRepository diapositiveRepository) {
        this.diapositiveRepository = diapositiveRepository;
    }

    /**
     * {@code POST  /diapositives} : Create a new diapositive.
     *
     * @param diapositive the diapositive to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new diapositive, or with status {@code 400 (Bad Request)} if the diapositive has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/diapositives")
    public ResponseEntity<Diapositive> createDiapositive(@Valid @RequestBody Diapositive diapositive) throws URISyntaxException {
        log.debug("REST request to save Diapositive : {}", diapositive);
        if (diapositive.getId() != null) {
            throw new BadRequestAlertException("A new diapositive cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Diapositive result = diapositiveRepository.save(diapositive);
        return ResponseEntity.created(new URI("/api/diapositives/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /diapositives} : Updates an existing diapositive.
     *
     * @param diapositive the diapositive to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated diapositive,
     * or with status {@code 400 (Bad Request)} if the diapositive is not valid,
     * or with status {@code 500 (Internal Server Error)} if the diapositive couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/diapositives")
    public ResponseEntity<Diapositive> updateDiapositive(@Valid @RequestBody Diapositive diapositive) throws URISyntaxException {
        log.debug("REST request to update Diapositive : {}", diapositive);
        if (diapositive.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Diapositive result = diapositiveRepository.save(diapositive);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, diapositive.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /diapositives} : get all the diapositives.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of diapositives in body.
     */
    @GetMapping("/diapositives")
    public List<Diapositive> getAllDiapositives() {
        log.debug("REST request to get all Diapositives");
        return diapositiveRepository.findAll();
    }

    /**
     * {@code GET  /diapositives/:id} : get the "id" diapositive.
     *
     * @param id the id of the diapositive to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the diapositive, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/diapositives/{id}")
    public ResponseEntity<Diapositive> getDiapositive(@PathVariable Long id) {
        log.debug("REST request to get Diapositive : {}", id);
        Optional<Diapositive> diapositive = diapositiveRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(diapositive);
    }

    /**
     * {@code DELETE  /diapositives/:id} : delete the "id" diapositive.
     *
     * @param id the id of the diapositive to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/diapositives/{id}")
    public ResponseEntity<Void> deleteDiapositive(@PathVariable Long id) {
        log.debug("REST request to delete Diapositive : {}", id);
        diapositiveRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
