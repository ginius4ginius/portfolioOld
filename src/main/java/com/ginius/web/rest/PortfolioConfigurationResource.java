package com.ginius.web.rest;

import com.ginius.domain.PortfolioConfiguration;
import com.ginius.repository.PortfolioConfigurationRepository;
import com.ginius.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ginius.domain.PortfolioConfiguration}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PortfolioConfigurationResource {

    private final Logger log = LoggerFactory.getLogger(PortfolioConfigurationResource.class);

    private static final String ENTITY_NAME = "portfolioConfiguration";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PortfolioConfigurationRepository portfolioConfigurationRepository;

    public PortfolioConfigurationResource(PortfolioConfigurationRepository portfolioConfigurationRepository) {
        this.portfolioConfigurationRepository = portfolioConfigurationRepository;
    }

    /**
     * {@code POST  /portfolio-configurations} : Create a new portfolioConfiguration.
     *
     * @param portfolioConfiguration the portfolioConfiguration to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new portfolioConfiguration, or with status {@code 400 (Bad Request)} if the portfolioConfiguration has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/portfolio-configurations")
    public ResponseEntity<PortfolioConfiguration> createPortfolioConfiguration(@Valid @RequestBody PortfolioConfiguration portfolioConfiguration) throws URISyntaxException {
        log.debug("REST request to save PortfolioConfiguration : {}", portfolioConfiguration);
        if (portfolioConfiguration.getId() != null) {
            throw new BadRequestAlertException("A new portfolioConfiguration cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PortfolioConfiguration result = portfolioConfigurationRepository.save(portfolioConfiguration);
        return ResponseEntity.created(new URI("/api/portfolio-configurations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /portfolio-configurations} : Updates an existing portfolioConfiguration.
     *
     * @param portfolioConfiguration the portfolioConfiguration to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated portfolioConfiguration,
     * or with status {@code 400 (Bad Request)} if the portfolioConfiguration is not valid,
     * or with status {@code 500 (Internal Server Error)} if the portfolioConfiguration couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/portfolio-configurations")
    public ResponseEntity<PortfolioConfiguration> updatePortfolioConfiguration(@Valid @RequestBody PortfolioConfiguration portfolioConfiguration) throws URISyntaxException {
        log.debug("REST request to update PortfolioConfiguration : {}", portfolioConfiguration);
        if (portfolioConfiguration.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PortfolioConfiguration result = portfolioConfigurationRepository.save(portfolioConfiguration);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, portfolioConfiguration.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /portfolio-configurations} : get all the portfolioConfigurations.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of portfolioConfigurations in body.
     */
    @GetMapping("/portfolio-configurations")
    public List<PortfolioConfiguration> getAllPortfolioConfigurations() {
        log.debug("REST request to get all PortfolioConfigurations");
        return portfolioConfigurationRepository.findAll();
    }

    /**
     * {@code GET  /portfolio-configurations/:id} : get the "id" portfolioConfiguration.
     *
     * @param id the id of the portfolioConfiguration to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the portfolioConfiguration, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/portfolio-configurations/{id}")
    public ResponseEntity<PortfolioConfiguration> getPortfolioConfiguration(@PathVariable Long id) {
        log.debug("REST request to get PortfolioConfiguration : {}", id);
        Optional<PortfolioConfiguration> portfolioConfiguration = portfolioConfigurationRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(portfolioConfiguration);
    }

    /**
     * {@code DELETE  /portfolio-configurations/:id} : delete the "id" portfolioConfiguration.
     *
     * @param id the id of the portfolioConfiguration to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/portfolio-configurations/{id}")
    public ResponseEntity<Void> deletePortfolioConfiguration(@PathVariable Long id) {
        log.debug("REST request to delete PortfolioConfiguration : {}", id);
        portfolioConfigurationRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
