package com.ginius.web.rest;

import com.ginius.domain.Diapositive;
import com.ginius.domain.PortfolioConfiguration;
import com.ginius.domain.Projet;
import com.ginius.repository.DiapositiveRepository;
import com.ginius.repository.PortfolioConfigurationRepository;
import com.ginius.repository.ProjetRepository;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/interface")
@Transactional
@CrossOrigin
public class InterfaceResource {

    @Autowired
    private PortfolioConfigurationRepository confRepository;

    @Autowired
    private ProjetRepository projetRepository;

    @Autowired
    private DiapositiveRepository diapositiveRepository;

    private final Logger log = LoggerFactory.getLogger(InterfaceResource.class);

    /**
     * {@code GET  /equipes} : get all the equipes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the configuration in body.
     */
    @GetMapping("/config")
    public PortfolioConfiguration getConfig() {
        log.debug("REST request to get the PortfolioConfiguration");
        List<PortfolioConfiguration> liste = confRepository.findAll();
        //seulement le premier élément est retourné
        System.out.println(liste.get(0).toString());

        return liste.get(0);

    }

    /**
     * {@code GET  /equipes} : get all the projects.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the configuration in body.
     */
    @GetMapping("/realisation")
    public List<Projet> getRealisations() {
        log.debug("REST request to get the projects");
        return projetRepository.findAll();


    }

    /**
     * {@code GET  /equipes} : get ONE project.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the configuration in body.
     */
    @GetMapping("/realisation/{id}")
    public ResponseEntity<Projet> getProject(@PathVariable Long id) {
        log.debug("REST request to get the project");
        Optional<Projet> projet = projetRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(projet);

    }

    /**
     * {@code GET  /equipes} : get dipositives from one project.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the configuration in body.
     */
    @GetMapping("/diapositive/{id}")
    public List<Diapositive> getDiapositiveFromProjectId(@PathVariable Long id) {
        log.debug("REST request to get the project");
        return diapositiveRepository.findAllByProjetId(id);

    }

}
