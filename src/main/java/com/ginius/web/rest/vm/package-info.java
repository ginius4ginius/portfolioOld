/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ginius.web.rest.vm;
