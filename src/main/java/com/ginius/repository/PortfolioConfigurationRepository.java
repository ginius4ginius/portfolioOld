package com.ginius.repository;

import com.ginius.domain.PortfolioConfiguration;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PortfolioConfiguration entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PortfolioConfigurationRepository extends JpaRepository<PortfolioConfiguration, Long> {

}
