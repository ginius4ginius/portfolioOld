package com.ginius.repository;

import com.ginius.domain.Diapositive;
import com.ginius.domain.Projet;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Diapositive entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DiapositiveRepository extends JpaRepository<Diapositive, Long> {

    List<Diapositive> findAllByProjetId(Long id);

}
