package com.ginius.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Projet.
 */
@Entity
@Table(name = "projet")
public class Projet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 2, max = 45)
    @Column(name = "nom", length = 45, nullable = false)
    private String nom;

    @Size(min = 2, max = 2000)
    @Column(name = "decription", length = 2000)
    private String decription;

    @Size(min = 2, max = 2000)
    @Column(name = "environnement", length = 2000)
    private String environnement;

    @NotNull
    @Column(name = "actif", nullable = false)
    private Boolean actif;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Projet nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDecription() {
        return decription;
    }

    public Projet decription(String decription) {
        this.decription = decription;
        return this;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

    public String getEnvironnement() {
        return environnement;
    }

    public Projet environnement(String environnement) {
        this.environnement = environnement;
        return this;
    }

    public void setEnvironnement(String environnement) {
        this.environnement = environnement;
    }

    public Boolean isActif() {
        return actif;
    }

    public Projet actif(Boolean actif) {
        this.actif = actif;
        return this;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Projet)) {
            return false;
        }
        return id != null && id.equals(((Projet) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Projet{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", decription='" + getDecription() + "'" +
            ", environnement='" + getEnvironnement() + "'" +
            ", actif='" + isActif() + "'" +
            "}";
    }
}
