package com.ginius.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Diapositive.
 */
@Entity
@Table(name = "diapositive")
public class Diapositive implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 2, max = 45)
    @Column(name = "photo_url", length = 45, nullable = false)
    private String photoUrl;

    @NotNull
    @Size(min = 2, max = 2000)
    @Column(name = "contenu", length = 2000, nullable = false)
    private String contenu;

    @ManyToOne
    @JsonIgnoreProperties("diapositives")
    private Projet projet;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public Diapositive photoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
        return this;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getContenu() {
        return contenu;
    }

    public Diapositive contenu(String contenu) {
        this.contenu = contenu;
        return this;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Projet getProjet() {
        return projet;
    }

    public Diapositive projet(Projet projet) {
        this.projet = projet;
        return this;
    }

    public void setProjet(Projet projet) {
        this.projet = projet;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Diapositive)) {
            return false;
        }
        return id != null && id.equals(((Diapositive) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Diapositive{" +
            "id=" + getId() +
            ", photoUrl='" + getPhotoUrl() + "'" +
            ", contenu='" + getContenu() + "'" +
            "}";
    }
}
