package com.ginius.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A PortfolioConfiguration.
 */
@Entity
@Table(name = "portfolio_configuration")
public class PortfolioConfiguration implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(min = 2, max = 45)
    @Column(name = "cv_url", length = 45)
    private String cvUrl;

    @NotNull
    @Size(min = 2, max = 255)
    @Column(name = "email", length = 255, nullable = false)
    private String email;

    @Size(min = 2, max = 2000)
    @Column(name = "presentation")
    private String presentation;

    @Column(name = "profile_url")
    private String profileUrl;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCvUrl() {
        return cvUrl;
    }

    public PortfolioConfiguration cvUrl(String cvUrl) {
        this.cvUrl = cvUrl;
        return this;
    }

    public void setCvUrl(String cvUrl) {
        this.cvUrl = cvUrl;
    }

    public String getEmail() {
        return email;
    }

    public PortfolioConfiguration email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPresentation() {
        return presentation;
    }

    public PortfolioConfiguration presentation(String presentation) {
        this.presentation = presentation;
        return this;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public PortfolioConfiguration profileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
        return this;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PortfolioConfiguration)) {
            return false;
        }
        return id != null && id.equals(((PortfolioConfiguration) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "PortfolioConfiguration{" +
            "id=" + getId() +
            ", cvUrl='" + getCvUrl() + "'" +
            ", email='" + getEmail() + "'" +
            ", presentation='" + getPresentation() + "'" +
            ", profileUrl='" + getProfileUrl() + "'" +
            "}";
    }
}
