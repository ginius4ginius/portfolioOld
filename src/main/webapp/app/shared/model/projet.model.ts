export interface IProjet {
  id?: number;
  nom?: string;
  decription?: string;
  environnement?: string;
  actif?: boolean;
}

export class Projet implements IProjet {
  constructor(public id?: number, public nom?: string, public decription?: string, public environnement?: string, public actif?: boolean) {
    this.actif = this.actif || false;
  }
}
