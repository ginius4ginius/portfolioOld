import { IProjet } from 'app/shared/model/projet.model';

export interface IDiapositive {
  id?: number;
  photoUrl?: string;
  contenu?: string;
  projet?: IProjet;
}

export class Diapositive implements IDiapositive {
  constructor(public id?: number, public photoUrl?: string, public contenu?: string, public projet?: IProjet) {}
}
