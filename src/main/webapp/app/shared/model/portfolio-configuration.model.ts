export interface IPortfolioConfiguration {
  id?: number;
  cvUrl?: string;
  email?: string;
  presentation?: string;
  profileUrl?: string;
}

export class PortfolioConfiguration implements IPortfolioConfiguration {
  constructor(public id?: number, public cvUrl?: string, public email?: string, public presentation?: string, public profileUrl?: string) {}
}
