import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'portfolio-configuration',
        loadChildren: () =>
          import('./portfolio-configuration/portfolio-configuration.module').then(m => m.PortfolioPortfolioConfigurationModule)
      },
      {
        path: 'projet',
        loadChildren: () => import('./projet/projet.module').then(m => m.PortfolioProjetModule)
      },
      {
        path: 'diapositive',
        loadChildren: () => import('./diapositive/diapositive.module').then(m => m.PortfolioDiapositiveModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class PortfolioEntityModule {}
