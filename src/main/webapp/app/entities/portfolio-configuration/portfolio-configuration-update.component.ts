import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPortfolioConfiguration, PortfolioConfiguration } from 'app/shared/model/portfolio-configuration.model';
import { PortfolioConfigurationService } from './portfolio-configuration.service';

@Component({
  selector: 'jhi-portfolio-configuration-update',
  templateUrl: './portfolio-configuration-update.component.html'
})
export class PortfolioConfigurationUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    cvUrl: [null, [Validators.minLength(2), Validators.maxLength(45)]],
    email: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(255)]],
    presentation: [],
    profileUrl: []
  });

  constructor(
    protected portfolioConfigurationService: PortfolioConfigurationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ portfolioConfiguration }) => {
      this.updateForm(portfolioConfiguration);
    });
  }

  updateForm(portfolioConfiguration: IPortfolioConfiguration): void {
    this.editForm.patchValue({
      id: portfolioConfiguration.id,
      cvUrl: portfolioConfiguration.cvUrl,
      email: portfolioConfiguration.email,
      presentation: portfolioConfiguration.presentation,
      profileUrl: portfolioConfiguration.profileUrl
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const portfolioConfiguration = this.createFromForm();
    if (portfolioConfiguration.id !== undefined) {
      this.subscribeToSaveResponse(this.portfolioConfigurationService.update(portfolioConfiguration));
    } else {
      this.subscribeToSaveResponse(this.portfolioConfigurationService.create(portfolioConfiguration));
    }
  }

  private createFromForm(): IPortfolioConfiguration {
    return {
      ...new PortfolioConfiguration(),
      id: this.editForm.get(['id'])!.value,
      cvUrl: this.editForm.get(['cvUrl'])!.value,
      email: this.editForm.get(['email'])!.value,
      presentation: this.editForm.get(['presentation'])!.value,
      profileUrl: this.editForm.get(['profileUrl'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPortfolioConfiguration>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
