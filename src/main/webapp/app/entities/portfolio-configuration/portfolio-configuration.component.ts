import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPortfolioConfiguration } from 'app/shared/model/portfolio-configuration.model';
import { PortfolioConfigurationService } from './portfolio-configuration.service';
import { PortfolioConfigurationDeleteDialogComponent } from './portfolio-configuration-delete-dialog.component';

@Component({
  selector: 'jhi-portfolio-configuration',
  templateUrl: './portfolio-configuration.component.html'
})
export class PortfolioConfigurationComponent implements OnInit, OnDestroy {
  portfolioConfigurations?: IPortfolioConfiguration[];
  eventSubscriber?: Subscription;

  constructor(
    protected portfolioConfigurationService: PortfolioConfigurationService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.portfolioConfigurationService.query().subscribe((res: HttpResponse<IPortfolioConfiguration[]>) => {
      this.portfolioConfigurations = res.body ? res.body : [];
    });
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPortfolioConfigurations();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPortfolioConfiguration): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInPortfolioConfigurations(): void {
    this.eventSubscriber = this.eventManager.subscribe('portfolioConfigurationListModification', () => this.loadAll());
  }

  delete(portfolioConfiguration: IPortfolioConfiguration): void {
    const modalRef = this.modalService.open(PortfolioConfigurationDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.portfolioConfiguration = portfolioConfiguration;
  }
}
