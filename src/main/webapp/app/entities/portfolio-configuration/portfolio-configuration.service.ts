import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IPortfolioConfiguration } from 'app/shared/model/portfolio-configuration.model';

type EntityResponseType = HttpResponse<IPortfolioConfiguration>;
type EntityArrayResponseType = HttpResponse<IPortfolioConfiguration[]>;

@Injectable({ providedIn: 'root' })
export class PortfolioConfigurationService {
  public resourceUrl = SERVER_API_URL + 'api/portfolio-configurations';

  constructor(protected http: HttpClient) {}

  create(portfolioConfiguration: IPortfolioConfiguration): Observable<EntityResponseType> {
    return this.http.post<IPortfolioConfiguration>(this.resourceUrl, portfolioConfiguration, { observe: 'response' });
  }

  update(portfolioConfiguration: IPortfolioConfiguration): Observable<EntityResponseType> {
    return this.http.put<IPortfolioConfiguration>(this.resourceUrl, portfolioConfiguration, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPortfolioConfiguration>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPortfolioConfiguration[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
