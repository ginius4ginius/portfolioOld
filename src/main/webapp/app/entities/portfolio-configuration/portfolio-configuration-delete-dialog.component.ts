import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPortfolioConfiguration } from 'app/shared/model/portfolio-configuration.model';
import { PortfolioConfigurationService } from './portfolio-configuration.service';

@Component({
  templateUrl: './portfolio-configuration-delete-dialog.component.html'
})
export class PortfolioConfigurationDeleteDialogComponent {
  portfolioConfiguration?: IPortfolioConfiguration;

  constructor(
    protected portfolioConfigurationService: PortfolioConfigurationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.portfolioConfigurationService.delete(id).subscribe(() => {
      this.eventManager.broadcast('portfolioConfigurationListModification');
      this.activeModal.close();
    });
  }
}
