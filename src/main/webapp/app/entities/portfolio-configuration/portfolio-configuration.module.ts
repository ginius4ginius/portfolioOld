import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PortfolioSharedModule } from 'app/shared/shared.module';
import { PortfolioConfigurationComponent } from './portfolio-configuration.component';
import { PortfolioConfigurationDetailComponent } from './portfolio-configuration-detail.component';
import { PortfolioConfigurationUpdateComponent } from './portfolio-configuration-update.component';
import { PortfolioConfigurationDeleteDialogComponent } from './portfolio-configuration-delete-dialog.component';
import { portfolioConfigurationRoute } from './portfolio-configuration.route';

@NgModule({
  imports: [PortfolioSharedModule, RouterModule.forChild(portfolioConfigurationRoute)],
  declarations: [
    PortfolioConfigurationComponent,
    PortfolioConfigurationDetailComponent,
    PortfolioConfigurationUpdateComponent,
    PortfolioConfigurationDeleteDialogComponent
  ],
  entryComponents: [PortfolioConfigurationDeleteDialogComponent]
})
export class PortfolioPortfolioConfigurationModule {}
