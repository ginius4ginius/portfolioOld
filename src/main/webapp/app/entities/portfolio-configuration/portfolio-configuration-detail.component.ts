import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPortfolioConfiguration } from 'app/shared/model/portfolio-configuration.model';

@Component({
  selector: 'jhi-portfolio-configuration-detail',
  templateUrl: './portfolio-configuration-detail.component.html'
})
export class PortfolioConfigurationDetailComponent implements OnInit {
  portfolioConfiguration: IPortfolioConfiguration | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ portfolioConfiguration }) => {
      this.portfolioConfiguration = portfolioConfiguration;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
