import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IPortfolioConfiguration, PortfolioConfiguration } from 'app/shared/model/portfolio-configuration.model';
import { PortfolioConfigurationService } from './portfolio-configuration.service';
import { PortfolioConfigurationComponent } from './portfolio-configuration.component';
import { PortfolioConfigurationDetailComponent } from './portfolio-configuration-detail.component';
import { PortfolioConfigurationUpdateComponent } from './portfolio-configuration-update.component';

@Injectable({ providedIn: 'root' })
export class PortfolioConfigurationResolve implements Resolve<IPortfolioConfiguration> {
  constructor(private service: PortfolioConfigurationService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPortfolioConfiguration> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((portfolioConfiguration: HttpResponse<PortfolioConfiguration>) => {
          if (portfolioConfiguration.body) {
            return of(portfolioConfiguration.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PortfolioConfiguration());
  }
}

export const portfolioConfigurationRoute: Routes = [
  {
    path: '',
    component: PortfolioConfigurationComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'portfolioApp.portfolioConfiguration.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PortfolioConfigurationDetailComponent,
    resolve: {
      portfolioConfiguration: PortfolioConfigurationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'portfolioApp.portfolioConfiguration.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PortfolioConfigurationUpdateComponent,
    resolve: {
      portfolioConfiguration: PortfolioConfigurationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'portfolioApp.portfolioConfiguration.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PortfolioConfigurationUpdateComponent,
    resolve: {
      portfolioConfiguration: PortfolioConfigurationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'portfolioApp.portfolioConfiguration.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
