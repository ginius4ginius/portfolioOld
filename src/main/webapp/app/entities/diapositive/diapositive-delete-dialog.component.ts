import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDiapositive } from 'app/shared/model/diapositive.model';
import { DiapositiveService } from './diapositive.service';

@Component({
  templateUrl: './diapositive-delete-dialog.component.html'
})
export class DiapositiveDeleteDialogComponent {
  diapositive?: IDiapositive;

  constructor(
    protected diapositiveService: DiapositiveService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.diapositiveService.delete(id).subscribe(() => {
      this.eventManager.broadcast('diapositiveListModification');
      this.activeModal.close();
    });
  }
}
