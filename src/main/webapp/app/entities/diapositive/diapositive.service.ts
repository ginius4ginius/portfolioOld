import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDiapositive } from 'app/shared/model/diapositive.model';

type EntityResponseType = HttpResponse<IDiapositive>;
type EntityArrayResponseType = HttpResponse<IDiapositive[]>;

@Injectable({ providedIn: 'root' })
export class DiapositiveService {
  public resourceUrl = SERVER_API_URL + 'api/diapositives';

  constructor(protected http: HttpClient) {}

  create(diapositive: IDiapositive): Observable<EntityResponseType> {
    return this.http.post<IDiapositive>(this.resourceUrl, diapositive, { observe: 'response' });
  }

  update(diapositive: IDiapositive): Observable<EntityResponseType> {
    return this.http.put<IDiapositive>(this.resourceUrl, diapositive, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDiapositive>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDiapositive[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
