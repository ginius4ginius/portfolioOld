import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDiapositive } from 'app/shared/model/diapositive.model';

@Component({
  selector: 'jhi-diapositive-detail',
  templateUrl: './diapositive-detail.component.html'
})
export class DiapositiveDetailComponent implements OnInit {
  diapositive: IDiapositive | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ diapositive }) => {
      this.diapositive = diapositive;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
