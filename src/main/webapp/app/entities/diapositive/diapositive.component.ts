import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDiapositive } from 'app/shared/model/diapositive.model';
import { DiapositiveService } from './diapositive.service';
import { DiapositiveDeleteDialogComponent } from './diapositive-delete-dialog.component';

@Component({
  selector: 'jhi-diapositive',
  templateUrl: './diapositive.component.html'
})
export class DiapositiveComponent implements OnInit, OnDestroy {
  diapositives?: IDiapositive[];
  eventSubscriber?: Subscription;

  constructor(
    protected diapositiveService: DiapositiveService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.diapositiveService.query().subscribe((res: HttpResponse<IDiapositive[]>) => {
      this.diapositives = res.body ? res.body : [];
    });
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInDiapositives();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IDiapositive): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInDiapositives(): void {
    this.eventSubscriber = this.eventManager.subscribe('diapositiveListModification', () => this.loadAll());
  }

  delete(diapositive: IDiapositive): void {
    const modalRef = this.modalService.open(DiapositiveDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.diapositive = diapositive;
  }
}
