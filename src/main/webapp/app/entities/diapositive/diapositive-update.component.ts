import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IDiapositive, Diapositive } from 'app/shared/model/diapositive.model';
import { DiapositiveService } from './diapositive.service';
import { IProjet } from 'app/shared/model/projet.model';
import { ProjetService } from 'app/entities/projet/projet.service';

@Component({
  selector: 'jhi-diapositive-update',
  templateUrl: './diapositive-update.component.html'
})
export class DiapositiveUpdateComponent implements OnInit {
  isSaving = false;

  projets: IProjet[] = [];

  editForm = this.fb.group({
    id: [],
    photoUrl: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(45)]],
    contenu: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(2000)]],
    projet: []
  });

  constructor(
    protected diapositiveService: DiapositiveService,
    protected projetService: ProjetService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ diapositive }) => {
      this.updateForm(diapositive);

      this.projetService
        .query()
        .pipe(
          map((res: HttpResponse<IProjet[]>) => {
            return res.body ? res.body : [];
          })
        )
        .subscribe((resBody: IProjet[]) => (this.projets = resBody));
    });
  }

  updateForm(diapositive: IDiapositive): void {
    this.editForm.patchValue({
      id: diapositive.id,
      photoUrl: diapositive.photoUrl,
      contenu: diapositive.contenu,
      projet: diapositive.projet
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const diapositive = this.createFromForm();
    if (diapositive.id !== undefined) {
      this.subscribeToSaveResponse(this.diapositiveService.update(diapositive));
    } else {
      this.subscribeToSaveResponse(this.diapositiveService.create(diapositive));
    }
  }

  private createFromForm(): IDiapositive {
    return {
      ...new Diapositive(),
      id: this.editForm.get(['id'])!.value,
      photoUrl: this.editForm.get(['photoUrl'])!.value,
      contenu: this.editForm.get(['contenu'])!.value,
      projet: this.editForm.get(['projet'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDiapositive>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IProjet): any {
    return item.id;
  }
}
