import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PortfolioSharedModule } from 'app/shared/shared.module';
import { DiapositiveComponent } from './diapositive.component';
import { DiapositiveDetailComponent } from './diapositive-detail.component';
import { DiapositiveUpdateComponent } from './diapositive-update.component';
import { DiapositiveDeleteDialogComponent } from './diapositive-delete-dialog.component';
import { diapositiveRoute } from './diapositive.route';

@NgModule({
  imports: [PortfolioSharedModule, RouterModule.forChild(diapositiveRoute)],
  declarations: [DiapositiveComponent, DiapositiveDetailComponent, DiapositiveUpdateComponent, DiapositiveDeleteDialogComponent],
  entryComponents: [DiapositiveDeleteDialogComponent]
})
export class PortfolioDiapositiveModule {}
