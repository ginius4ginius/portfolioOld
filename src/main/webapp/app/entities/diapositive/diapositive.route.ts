import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IDiapositive, Diapositive } from 'app/shared/model/diapositive.model';
import { DiapositiveService } from './diapositive.service';
import { DiapositiveComponent } from './diapositive.component';
import { DiapositiveDetailComponent } from './diapositive-detail.component';
import { DiapositiveUpdateComponent } from './diapositive-update.component';

@Injectable({ providedIn: 'root' })
export class DiapositiveResolve implements Resolve<IDiapositive> {
  constructor(private service: DiapositiveService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDiapositive> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((diapositive: HttpResponse<Diapositive>) => {
          if (diapositive.body) {
            return of(diapositive.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Diapositive());
  }
}

export const diapositiveRoute: Routes = [
  {
    path: '',
    component: DiapositiveComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'portfolioApp.diapositive.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DiapositiveDetailComponent,
    resolve: {
      diapositive: DiapositiveResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'portfolioApp.diapositive.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DiapositiveUpdateComponent,
    resolve: {
      diapositive: DiapositiveResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'portfolioApp.diapositive.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DiapositiveUpdateComponent,
    resolve: {
      diapositive: DiapositiveResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'portfolioApp.diapositive.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
